//
//  ViewController.swift
//  colorMaker-sliders
//
//  Created by  Ahmed Shendy on 9/3/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: Properties
    
    let defaultAlpha = CGFloat(100.0)
    
    //MARK: Outlet Properties
    
    @IBOutlet var redSlider: UISlider!
    @IBOutlet var greenSlider: UISlider!
    @IBOutlet var blueSlider: UISlider!
    @IBOutlet var showBox: UIView!

    //MARK: Application Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateShowBoxColor()
    }

    //MARK: Action Methods
    
    @IBAction func updateShowBoxColor() {
        let red: CGFloat = CGFloat(redSlider.value)
        let green: CGFloat = CGFloat(greenSlider.value)
        let blue: CGFloat = CGFloat(blueSlider.value)
        
        showBox.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: defaultAlpha)
    }
}

